<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Category;
use App\Entity\Article;
use App\Repository\CategoryRepository;
use App\Form\CategoryType;

/**
 * @Route("/admin/category")
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/list", name="list_category")
     */
    public function list_category(CategoryRepository $repoCategory): Response
    {
        $categories = $repoCategory->findAll();

        return $this->render('category/list.category.html.twig', ['categories' => $categories]);
    }

    /**
     * @Route("/new", name="create_category")
     */
    public function create_category(Request $request)
    {
        $new_category = new Category();

        $form = $this->createForm(CategoryType::class, $new_category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($new_category);
            $manager->flush();

            return $this->redirectToRoute('list_category');
        }
        return $this->render('category/create.category.html.twig', ['formCategory' => $form->createView(), 'action' => true]);
    }

    /**
     * @Route("/{id}/edit", name="edit_category")
     */
    public function edit_category(Category $categorie, Request $request)
    {

        $form = $this->createForm(CategoryType::class, $categorie); 

        $form->handleRequest($request); 

        if($form->isSubmitted() && $form->isValid())
        {
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($categorie);
            $manager->flush();

            return $this->redirectToRoute('list_category');
        }

        return $this->render('category/create.category.html.twig', ['formCategory' => $form->createView(), 'action' => false]);
    }

    /**
     * @Route("/{id}/delete", name="delete_category")
     */
    public function delete_category(CategoryRepository $repoCategory, $id)
    {   
        $categorie = $repoCategory->find($id);

        $manager = $this->getDoctrine()->getManager();

        $manager->remove($categorie);

        $manager->flush();
        
        return $this->redirectToRoute('list_category');
    }
}
