<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use App\Repository\CommentRepository;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/user/blog")
 */
class BlogUserController extends AbstractController
{
    /**
     * @Route("/home", name="home_user")
     */
    public function list_article(Request $request, PaginatorInterface $paginator, CommentRepository $repoComments, ArticleRepository $repoArticle, CategoryRepository $repoCategory): Response
    {   
        $liste_articles = $repoArticle->findValidArticles();

        $articles_paginations = $paginator->paginate($liste_articles, $request->query->getInt('page', 1), 5);

        $commentaires_valides = $repoComments->findBy(array('valid' => 1), array('createdAt' => 'DESC'), 5);

        $liste_categories = $repoCategory->findAll();

        return $this->render('blog_user/index.html.twig', ['commentaires_valides' => $commentaires_valides, 'articles_paginations' => $articles_paginations, 'categories' => $liste_categories]);
    }

    /**
     * @Route("/{id}/{slug}", name="blog_show_user")
     */
    public function show_article(ArticleRepository $repoArticle, $id)
    {   
        $article = $repoArticle->find($id);
        
        return $this->render('blog_user/show.user.html.twig', ['article' => $article]);
    }
}
