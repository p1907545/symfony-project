<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\AsciiSlugger;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;

/**
 * @Route("/admin")
 */
class BlogAdminController extends AbstractController
{
    /**
     * @Route("/blog/list", name="blog_list_admin")
     */
    public function liste_article(ArticleRepository $repoArticle): Response
    {   
        $liste_articles = $repoArticle->findBy(array() ,array('createdAt' => 'DESC'), null);

        return $this->render('blog_admin/index.html.twig', ['articles' => $liste_articles]);
    }

    /**
     * @Route("/", name="home_admin")
     */
    public function homepage_admin()
    {
        return $this->render('blog_admin/home.html.twig');
    }

    /**
     * @Route("/blog/new", name="blog_create")
     */
    public function create_article(Request $request)
    {
        $new_article = new Article();

        $form = $this->createForm(ArticleType::class, $new_article);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $new_article->setCreatedAt(new \DateTime());
            $new_article->setUpdatedAt(new \DateTime());
            $new_article->setPublishedAt(new \DateTime());
            $new_article->setImage('http://placehold.it/150x150');

            $slugger = new AsciiSlugger();
            $slug = $slugger->slug($new_article->getTitle());
            $new_article->setSlug($slug);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($new_article);
            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $new_article->getId()]);
        }

        return $this->render('blog_admin/create.html.twig', ['formArticle' => $form->createView(), 'action' => false]);
    }

    /**
     * @Route("/blog/{id}/edit", name="blog_edit")
     */
    public function edit_article(Article $article, Request $request): Response
    {   
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $article->setUpdatedAt(new \DateTime());
            $article->setImage('http://placehold.it/150x150');

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        }

        return $this->render('blog_admin/create.html.twig', ['formArticle' => $form->createView(), 'action' => true]);
    }

    /**
     * @Route("/blog/{id}/show", name="blog_show")
     */
    public function show_article(ArticleRepository $repoArticle, $id)
    {   
        $article = $repoArticle->find($id);

        return $this->render('blog_admin/show.html.twig', ['article' => $article]);
    }

    /**
     * @Route("/blog/{id}/delete", name="blog_delete")
     */
    public function delete_article(ArticleRepository $repoArticle, $id)
    {   
        $article = $repoArticle->find($id);

        $manager = $this->getDoctrine()->getManager();

        $manager->remove($article); 

        $manager->flush();
        
        return $this->redirectToRoute('blog_list_admin');
    }
}
