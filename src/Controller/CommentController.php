<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Comment;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use App\Form\ArticleType;
use App\Form\CommentType;

class CommentController extends AbstractController
{
    /**
     * @Route("/admin/{id}/comment/validation", name="comment_validation")
     */
    public function validate_comment(CommentRepository $repoComment, $id): Response
    {
        /** @var Comment $comment */
        $comment = $repoComment->find($id);
        $manager = $this->getDoctrine()->getManager();

        if($comment !== null) 
        {
            $comment->setValid(true);
            $manager->persist($comment);
            $manager->flush();
        }
        
        return $this->redirectToRoute('blog_list_admin'); 
    }

     /**
     * @Route("/admin/{id}/comment/desactivation", name="comment_desactivation")
     */
    public function desactivate_comment(CommentRepository $repoComment, $id): Response
    {
        /** @var Comment $comment */
        $comment = $repoComment->find($id);
        $manager = $this->getDoctrine()->getManager();

        if($comment !== null) 
        {
            $comment->setValid(false);
            $manager->persist($comment);
            $manager->flush();
        }
        
        return $this->redirectToRoute('blog_list_admin'); 
    }

    /**
     * @Route("/admin/{id}/comment/delete", name="comment_delete")
     */
    public function delete_comment(CommentRepository $repoComment, $id): Response
    {
        $comment = $repoComment->find($id);

        $manager = $this->getDoctrine()->getManager();

        if ($comment !== null) 
        {
            $manager->remove($comment);
            $manager->flush();
        }
        
        return $this->redirectToRoute('blog_list_admin'); 
    }

    /**
     * @Route("user/add/{id}/comment", name="add_comment")
     */
    public function add_comment(Request $request, ArticleRepository $repoArticle, $id): Response
    {
        $new_commentaire = new Comment();

        $article = $repoArticle->find($id);

        $form = $this->createForm(CommentType::class, $new_commentaire);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $manager = $this->getDoctrine()->getManager();

            $new_commentaire->setValid(false);
            $new_commentaire->setCreatedAt(new \DateTime());
            $new_commentaire->setArticle($article);

            $manager->persist($new_commentaire);
            $manager->flush();

            return $this->redirectToRoute('home_user');
        }
        return $this->render('comment/add.comment.html.twig', ['formComment' => $form->createView()]);
    }
}
