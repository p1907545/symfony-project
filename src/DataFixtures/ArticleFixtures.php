<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use Symfony\Component\String\Slugger\AsciiSlugger;

// les cripts Fixtures permettent de remplir les tables
// avec des fausses données pour utiliser les tables

// On va créer 3 catégories
// pour chaque catégorie, on va donner ses infos (titre + contenu)
// pour la meme catégorie, on va créer plusieurs articles
// pour chaque article, on va lui donner ses infos (contenu, etc..) + certains nb de commentaires

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = \Faker\Factory::create('fr_FR'); // génération de fausses données en français

        // créer 3 catégories fakées

        for($i=1; $i<=3; $i++)
        {
            $category = new Category();
            $category->setTitle($faker->sentence())
                    ->setDescription($faker->paragraph());

            $manager->persist($category);

            // créer entre 4 et 6 articles
            for($j=1; $j<=mt_rand(4,6); $j++) // mt_rand(4,6) = fonction PHP qui retourne un nombre au pffe entre 4 et 6
            {
                $article = new Article();

                $content = '<p>'.join($faker->paragraphs(5), '</p><p>').'</p>';

                $slugger = new AsciiSlugger();

                $article->setTitle($faker->sentence())
                        ->setContent($content) // il faut lui donner un string
                        ->setDescription($faker->sentence()) // il faut lui donner un string
                        ->setSlug($slugger->slug($article->getTitle()))
                        ->setImage($faker->imageUrl())
                        ->setCreatedAt($faker->dateTimeBetween('-6 months')) // minimum il y a 6 mois, maxi date de mtn
                        ->setUpdatedAt($faker->dateTimeBetween('-6 months')) // minimum il y a 6 mois, maxi date de mtn
                        ->setPublishedAt($faker->dateTimeBetween('-6 months')) // minimum il y a 6 mois, maxi date de mtn
                        ->addCategory($category);

                $manager->persist($article);

                // On donne des commentaires à l'articles
                for($k=1; $k<=mt_rand(4,10); $k++)
                {
                    $comment = new Comment();

                    $content = '<p>'.join($faker->paragraphs(2), '</p><p>').'</p>';

                    // petit algo pour éviter les incohérences des dates

                    $days = (new \DateTime())->diff($article->getCreatedAt())->days;

                    $comment->setAuthor($faker->name)
                            ->setContent($content)
                            ->setCreatedAt($faker->dateTimeBetween('-'.$days.'days'))
                            ->setValid(false)
                            ->setArticle($article);

                    $manager->persist($comment);
                }
            }
        }

        $manager->flush(); // envoie la requete SQL pour mettre à jour la BD
    }
}
